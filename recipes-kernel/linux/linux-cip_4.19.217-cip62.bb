#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.19.y-cip"

SRC_URI[sha256sum] = "00b22f2e7b3d4f5787ca92f2e182211ac2c1391c09435174c2b4670b2837b23c"
